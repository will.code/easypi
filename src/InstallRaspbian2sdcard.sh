#!/bin/bash

workingdir="/tmp"
sha256="$(curl -Ls https://www.raspberrypi.org/downloads/raspbian/ | grep -i "Raspbian Stretch Lite" -A 20 | grep -i "SHA-256:" | sed 's/.*strong>\(.*\)<\/strong.*/\1/g')"

if [ ! "$(dpkg -l | grep axel | grep "command line download accelerator")" ]
then
	sudo apt-get update && apt-get install -y axel
fi

carddrive="$(ls -la /dev/disk/by-id/ | egrep "usb|mmc" | grep -v "part" | sed 's/^.*\/\(.*\)$/\1/g' | tr -d '123456789')" # 0 is needed for mmc partitions
cardsize="$(sudo fdisk -l /dev/${carddrive} | head -n 1 | awk '{ print $3" "$4 }' | sed 's/,$//g')"
echo "-----------------------------------------------"
echo "SD Card device: /dev/${carddrive} with size ${cardsize}"
echo "-----------------------------------------------"

if [ ! "${carddrive}" ]				# only for USB card readers at the moment !
then
	echo "No SD Card found"
	exit 254
fi

for device in $(grep "${carddrive}" /proc/mounts | cut -d " " -f1)
do
	echo "DEVICE: ${device}"
	sudo umount -fl ${device} > /dev/null
done

if [ "$(grep -i "${carddrive}" /proc/mounts)" ]
then
	echo "ERROR: umount of ${device} not possible"
	exit 254
fi

sudo parted -s /dev/${carddrive} mktable msdos
sudo sync

if [ ! -f ${workingdir}/raspbian_lite_latest ]							# no image present
then
	echo "Downloading Raspbian Lite..."
	axel -a https://downloads.raspberrypi.org/raspbian_lite_latest -o ${workingdir}/raspbian_lite_latest
else
	if [ -f ${workingdir}/raspbian_lite_latest.st ]						# if download could NOT be resumed
	then
		echo "Resuming download of Raspbian Lite..."
		axel -a https://downloads.raspberrypi.org/raspbian_lite_latest -o ${workingdir}/raspbian_lite_latest
	else
		if [ "$(sha256sum ${workingdir}/raspbian_lite_latest | grep "${sha256}" | tr -d " ")" ]		# image already downloaded and up to date
		then
			echo "Raspbian Lite is already downloaded and up to date !"
		else															# image present but outdated
			echo "Deleting old Raspian Lite version and start downloading new version..."
			sudo rm -f ${workingdir}/raspbian_lite_* > /dev/null
			axel -a https://downloads.raspberrypi.org/raspbian_lite_latest -o ${workingdir}/raspbian_lite_latest
			if [ ! "$(sha256sum ${workingdir}/raspbian_lite_latest | grep "${sha256}" | tr -d " ")" ] 	# image downloaded, up to date but sha256 failed
			then
				echo "ERROR: SHA265 cecksum differs ! Download failed !!!"
				exit 354
			else														# up to date image successfully downloaded
				echo "Download successful."
			fi
		fi
	fi
fi

unzip -p ${workingdir}/raspbian_lite_latest | sudo dd of=/dev/${carddrive} bs=4M status=progress conv=fsync &&
sudo sync
sync

echo "Mounting SD-card..."
tmpmnt_dir=$(mktemp -d -t raspbian-XXXXXXXXXX)
cardpart2="$(blkid | grep "${carddrive}" | grep "2:" | cut -d ":" -f1)"

sudo mount ${cardpart2} ${tmpmnt_dir} &&

echo "Configuring SSHD..."
sudo sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/g' ${tmpmnt_dir}/etc/ssh/sshd_config
sudo sed -i 's/#UseDNS no/UseDNS no/g' ${tmpmnt_dir}/etc/ssh/sshd_config
#sed -i 's/#X11DisplayOffset 10/X11DisplayOffset 10/g' ${tmpmnt_dir}/etc/ssh/sshd_config
#sed -i 's/#X11UseLocalhost yes/X11UseLocalhost yes/g' ${tmpmnt_dir}/etc/ssh/sshd_config
sudo sed -i 's/^exit 0/systemctl start ssh; exit 0/g' ${tmpmnt_dir}/etc/rc.local
sudo sync
sync
echo "Umounting SD-card..."
sudo umount ${cardpart2} &&

echo "==============================================================="
echo "Please unplug the SD card. You can boot your RaspberryPi now..."
echo "==============================================================="

exit 0
