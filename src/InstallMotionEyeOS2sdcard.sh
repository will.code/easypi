#!/bin/bash

########################################################################
MEOS_release="20190119" 		# Enter your favorite version HERE (https://github.com/ccrisan/motioneyeos/releases)
raspi_model="raspberrypi3" 		# Please look here for a list: https://github.com/ccrisan/motioneyeos/releases/tag/[$MEOS_release]
workingdir="/tmp"

ip_address_netmask="10.1.50.5/24"
gateway="10.1.1.1"
dns_server="10.1.1.1"

########################################################################

sudo apt-get update && apt-get install -y curl wget xz-utils

carddrive="$(ls -la /dev/disk/by-id/ | egrep "usb|mmc" | grep -v "part" | sed 's/^.*\/\(.*\)$/\1/g' | tr -d '123456789')" # 0 is needed for mmc partitions
cardsize="$(sudo fdisk -l /dev/${carddrive} | head -n 1 | awk '{ print $3" "$4 }' | sed 's/,$//g')"
echo "-----------------------------------------------"
echo "SD Card device: /dev/${carddrive} with size ${cardsize}"
echo "-----------------------------------------------"

if [ ! "${carddrive}" ]				# only for USB card readers at the moment !
then
	echo "No SD Card found"
	exit 254
fi

for device in $(grep "${carddrive}" /proc/mounts | cut -d " " -f1)
do
	echo "DEVICE: ${device}"
	sudo umount -fl ${device} > /dev/null
done

if [ "$(grep -i "${carddrive}" /proc/mounts)" ]
then
	echo "ERROR: umount of ${device} not possible"
	exit 254
fi

sudo parted -s /dev/${carddrive} mktable msdos
sudo sync


image_url="$(curl -Ls https://github.com/ccrisan/motioneyeos/releases | grep -i "${MEOS_release}" | grep -i "${raspi_model}" | grep ccrisan | awk -F '"' '{ print $2 }')"
image_filename="$(basename $(echo "${image_url}"))"
	
if [ ! -f ${workingdir}/motioneyeos-${raspi_model}-${MEOS_release}.img ]
then
	wget -q -c https://github.com${image_url} -O ${workingdir}/${image_filename} --show-progress --progress=bar:force &&
	echo "Download successful."

	xz -v --decompress ${workingdir}/${image_filename} &&
	echo "Image extracted successfully."
fi

	wget -q -c https://raw.githubusercontent.com/ccrisan/motioneyeos/master/writeimage.sh -O ${workingdir}/writeimage.sh --show-progress --progress=bar:force &&
	chmod +x ${workingdir}/writeimage.sh
	echo "Imaging tool downloaded."

	echo "Writing image to SD card..."
	image_filename_decomp="$(echo "${image_filename}" | sed 's/\(.*\).xz$/\1/g')"
		
	sudo ${workingdir}/writeimage.sh -d /dev/${carddrive} -i "${workingdir}/${image_filename_decomp}" -s "${ip_address_netmask}:${gateway}:${dns_server}"

