#!/bin/bash

# execute script as user 'pi'

host_name="raspberrypi01"
ipaddress="10.1.1.4"
netmask="255.128.0.0"
gateway="10.1.1.1"
nameservers="10.1.1.1 fritz.box"

PIMATIC_PLUGINS=( 'pimatic-backup' 'pimatic-cron' 'pimatic-homeduino' 'pimatic-mobile-frontend' 'pimatic-netcheck' 'pimatic-onkyo' 'pimatic-snmp' 'pimatic-telegram' 'pimatic-wakeonlan' 'pimatic-yamaha-avr' 'sqlite3' )

# disable predictable network interface names
sudo ln -s -f /dev/null /etc/systemd/network/99-default.link

# enable ssh daemon
echo "Enabling SSHD..."
sudo systemctl enable ssh &&
sudo sed -i 's/^systemctl start ssh; exit 0/exit 0/g' ${tmpmnt_dir}/etc/rc.local # disables SSHD woraround

# configure network
if [ ! -f /etc/network/_interfaces_ORG ]
then
	echo "Configuring network..."
	sudo mv /etc/network/interfaces /etc/network/_interfaces_ORG

	cat <<- EOF | sudo tee -a /etc/network/interfaces
	# interfaces(5) file used by ifup(8) and ifdown(8)
	# Please note that this file is written to be used with dhcpcd
	# For static IP, consult /etc/dhcpcd.conf and 'man dhcpcd.conf'
	# Include files from /etc/network/interfaces.d:
	source-directory /etc/network/interfaces.d
	auto lo
	iface lo inet loopback
	#iface eth0 inet manual
	# Ethernet
	auto eth0
	#allow-hotplug eth0
	iface eth0 inet static
	address ${ipaddress}
	netmask ${netmask}
	#network 10.0.0.0/9
	#broadcast 10.127.255.255
	gateway ${gateway}
	dns-nameservers ${nameservers}
	# wlan0
	allow-hotplug wlan0
	iface wlan0 inet manual
		wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
	allow-hotplug wlan1
	iface wlan1 inet manual
		wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
	EOF

	sudo systemctl restart networking

fi

# upgrade system
sudo apt-get update

if [ "$(sudo apt-get upgrade -y -s | grep "The following packages will be upgraded")" ]
then
	echo "Upgrading system..."
	sudo apt-get upgrade -y
	echo "###########################################################"
	echo "I will reboot now. Please restart this script after login."
	echo "After reboot you can login to IP ${ipaddress} via ssh."
	echo "###########################################################"
	sleep 3
	sudo reboot
fi

# install packages
echo "Installing some packages..."
sudo apt-get install -y build-essential git mc wget curl autofs samba-common cifs-utils backup-manager

# NOPASSWD in sudoers
echo "Setting SUDO rights..."
sudo sed -i 's/^\(%sudo.*ALL=.*ALL:.*\)/#\1\n%sudo  ALL=(ALL) NOPASSWD: ALL/g' /etc/sudoers

echo "Creating useraccounts..."
sudo useradd -d /home/pimatic -m -G adm,dialout,cdrom,sudo,audio,video,plugdev,games,users,input,netdev,spi,i2c,gpio -p $6$iuMCu/w7$sYw9omoncE.6Dxld/dzjXVf0sX7yHfeWDSSXU30y5PDDEt4Ysti5yWaslyUaKOcGEliBe.hrjG//agztWMhpR1 -s /bin/bash -U -u 1002 pimatic
sudo useradd -d /home/pihole -m -G adm,dialout,cdrom,sudo,audio,video,plugdev,games,users,input,netdev,spi,i2c,gpio,www-data -p $6$S/XZwT6r$iRK9iHrG0ebxGwNM0No0B85WtVZ.95.eJ345rVm7Mued5DJjEWVcioVBuOl1Nry9eqBtQ5HTcAIvMaNXRTfPr0 -s /bin/bash -U -u 1003 pihole

# hostname
echo "Setting hostname"
sudo hostname ${host_name}
sudo sed -i 's/127.0.1.1.*/127.0.1.1\traspberrypi01/g' /etc/hosts               # Should be variable 'host_name' here :-(

# fstab
echo "Optimizing /etc/fstab..."
part1="$(blkid | grep "$(grep "/boot" /etc/fstab | grep "\-01" | cut -d " " -f1 | cut -d "=" -f2)" | cut -d ":" -f1)"
part2="$(blkid | grep "$(grep "/ " /etc/fstab | grep "\-02" | cut -d " " -f1 | cut -d "=" -f2)" | cut -d ":" -f1)"

sudo sed -i 's|.*=.*\(\s.\/boot.*\)|'"${part1}"'\t\1|g' /etc/fstab
sudo sed -i 's|.*=.*\(\s.\/ .*\)|'"${part2}"'\t\1|g' /etc/fstab

# sudo -H -u pimatic bash -c 'echo "I am $USER, with uid $UID"'

echo "Installing node.js..."
#Pi Model A, B, B+ or Zero
if [ "$(grep "model name" /proc/cpuinfo | uniq  | grep -i "armv6")" ]
then
	wget https://nodejs.org/dist/v4.9.1/node-v4.9.1-linux-armv6l.tar.gz -P /tmp
	sudo tar xzvf /tmp/node-v4.9.1-linux-armv6l.tar.gz --strip=1 -C /usr/local
# Pi 2 Mode B or Pi 3 Model B
elif [ "$(grep "model name" /proc/cpuinfo | uniq  | grep -i "armv7")" ]
then
	wget https://nodejs.org/dist/v4.9.1/node-v4.9.1-linux-armv7l.tar.gz -P /tmp
	sudo tar xzvf /tmp/node-v4.9.1-linux-armv7l.tar.gz --strip=1 -C /usr/local
else
	echo "No suitable RaspberryPi model found"
	exit 254
fi

if [ ! "$(/usr/bin/env node --version | grep "4.9.1")" ]
then
	echo "Wrong node.js version"
	exit 254
fi

echo "Installing PiMatic..."
sudo -H -u pimatic bash -c 'mkdir ~/pimatic-app'
sudo -H -u pimatic bash -c 'cd ~; npm install pimatic --prefix pimatic-app --production'
sudo -H -u pimatic bash -c 'cp ~/pimatic-app/node_modules/pimatic/config_default.json ~/pimatic-app/config.json'

# make pimatic available globally
sudo -H -u pimatic bash -c 'cd ~/pimatic-app/node_modules/pimatic; sudo npm link'

wget https://raw.githubusercontent.com/pimatic/pimatic/master/install/pimatic-init-d -P /tmp
sudo cp /tmp/pimatic-init-d /etc/init.d/pimatic
sudo chmod +x /etc/init.d/pimatic
sudo chown root:root /etc/init.d/pimatic
sudo update-rc.d pimatic defaults

# SSL configuration [missing]
# https://pimatic.teamemo.com/Guide/Remote-Access-and-SSL

# https://pimatic.teamemo.com/Guide/Running
# https://ehrenbach.spdns.org/root-ca-cert.crt

sudo -H -u pimatic bash -c 'sudo sudo rm -rf ~/pimatic-app/ca'                  # Fehler abfangen !!!
sudo -H -u pimatic bash -c 'wget https://raw.githubusercontent.com/pimatic/pimatic/master/install/ssl-setup -P /home/pimatic/pimatic-app/'
sudo -H -u pimatic bash -c 'chmod +x /home/pimatic/pimatic-app/ssl-setup'
sudo -H -u pimatic bash -c 'cd ~/pimatic-app; ./ssl-setup'
echo "###########################################################"
echo "After starting PiMatic service navigate your web browser to"
echo "http://your-ddns/root-ca-cert.crt"
echo "###########################################################"

# install plugins
for plugin in ${PIMATIC_PLUGINS[@]}
do
	sudo -H -u pimatic bash -c "cd ~/pimatic-app; sudo npm install --unsafe-perm ${plugin}"
done

# BACKUP
sudo mkdir /mnt/cifs

# Installation finished
echo "###########################################################"
echo "Finished. I will reboot now."
echo "After reboot you can login to IP ${ipaddress} via ssh."
echo "###########################################################"
sleep 3
sudo reboot
